import copy
import glob
import os
import os.path as osp
import time

import cv2
import numpy as np
from sklearn.cluster import DBSCAN
from sklearn import decomposition


def normalization(data):
    _range = np.max(data, axis=0, keepdims=True) - np.min(data, axis=0, keepdims=True)
    return np.array((data - np.min(data, axis=0, keepdims=True)) / _range * 255, dtype=np.uint8)


def add_rgb_feat(keypoint, desc, img):
    desc_pca = decomposition.PCA(n_components=3)
    desc_pca_result = desc_pca.fit_transform(desc)
    desc_pca_result = normalization(desc_pca_result)
    rgb_feat = np.zeros((desc.shape[0], 3), dtype=np.uint8)
    for i in range(len(keypoint)):
        rgb_feat[i] = img[int(keypoint[i].pt[1]), int(keypoint[i].pt[0])]
    feat = np.hstack((desc_pca_result, rgb_feat))
    return rgb_feat


class Matcher:
    def __init__(self):
        self.debug = True
        templates_path = osp.join(osp.split(osp.realpath(__file__))[0], 'templates')
        mask_list = glob.glob(osp.join(templates_path, 'mask', '*'))
        self.detect_keypoint_images = {}
        for mask_path in mask_list:
            img = cv2.imread(mask_path.replace('templates/mask', 'templates/image'))
            img_mask = cv2.imread(mask_path, 0)
            self.detect_keypoint_images[mask_path] = (img, img_mask)
        templates_len = len(self.detect_keypoint_images)
        self.keypoint2circle_center_dis_bound = [200, 400]
        self.cluster_min_points_count = 10 * templates_len
        self.dbscan_eps = 10
        self.dbscan_min_samples = 2 * templates_len
        self.keypoint_match_min_dis = 3.0
        self.circle_global_radius_bound = [100, 200, 400]

    def detect_local_circle(self, image, mask=False):
        cimage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)  # 灰度图
        image_mask = np.zeros_like(cimage)
        circles = cv2.HoughCircles(cimage, cv2.HOUGH_GRADIENT, 1, 100, param1=150, param2=100, minRadius=0,
                                   maxRadius=1000)
        circles = np.uint16(np.around(circles))  # 取整
        h, w = image.shape[:2]
        local_circle_center = None
        min_dis = float('inf')
        for center_i in circles[0, :]:
            local_circle_dis = np.sqrt((center_i[0] - w / 2) ** 2 + (center_i[1] - h / 2) ** 2)
            if local_circle_dis < min_dis:
                min_dis = local_circle_dis
                local_circle_center = center_i

        cv2.circle(image_mask, (local_circle_center[0], local_circle_center[1]), local_circle_center[2] + 30, 255, 35)
        if self.debug:
            image_show = copy.deepcopy(image)
            cv2.circle(image_show, (local_circle_center[0], local_circle_center[1]), local_circle_center[2] + 30,
                       (255, 0, 0), 30)
            cv2.imshow("row_circles", image_show)
            cv2.waitKey(0)
        if mask:
            return local_circle_center, image_mask
        else:
            return local_circle_center[:2]

    def detect_global_circle(self, image):
        cimage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)  # 灰度图
        h, w = cimage.shape
        circles = cv2.HoughCircles(cimage, cv2.HOUGH_GRADIENT, 1, 100, param1=150, param2=30, minRadius=0,
                                   maxRadius=100)
        circles = np.uint16(np.around(circles))
        circles = circles[0, :]
        circle_dis_to_image_center = []
        if len(circles) == 0:
            raise Exception('Global Circle Not Founded！')
        for circle in circles:
            circle_dis_to_image_center.append(np.linalg.norm((circle[0] - w / 2, circle[1] - h / 2)))
        circle_dis_to_center = np.array(circle_dis_to_image_center)
        center_circle_index = np.argmin(circle_dis_to_center)
        center_circle = circles[center_circle_index]
        circles = np.delete(circles, center_circle_index, axis=0)
        circle_dis_to_center_circle = []
        for circle in circles:
            circle_dis_to_center_circle.append(
                np.linalg.norm((int(circle[0]) - center_circle[0], int(circle[1]) - center_circle[1])))
        circle_dis_to_center_circle = np.array(circle_dis_to_center_circle)
        inner_circle = circles[np.where((circle_dis_to_center_circle > self.circle_global_radius_bound[0]) & (
                circle_dis_to_center_circle < self.circle_global_radius_bound[1]))]
        inner_circle[:, 2] = np.mean(inner_circle[:, 2])
        outer_circle = circles[np.where((circle_dis_to_center_circle > self.circle_global_radius_bound[1]) & (
                circle_dis_to_center_circle < self.circle_global_radius_bound[2]))]
        outer_circle[:, 2] = np.mean(outer_circle[:, 2])
        if self.debug:
            image_show = copy.deepcopy(image)
            cv2.circle(image_show, (center_circle[0], center_circle[1]), center_circle[2], (0, 255, 255), 2)
            for i in inner_circle:
                cv2.circle(image_show, (i[0], i[1]), i[2], (0, 0, 255), 2)
            for i in outer_circle:
                cv2.circle(image_show, (i[0], i[1]), i[2], (255, 0, 255), 2)
            cv2.imshow("row_circles", image_show)
            # name = str(time.time())
            # cv2.imwrite("./data/rui_hong_global_circle_vis/" + name + '.png', image_show)
            cv2.waitKey(0)
        return tuple((center_circle, inner_circle, outer_circle))

    def detect_local_keypoint(self, image):
        image_color = copy.deepcopy(image)
        image_dst_gray = cv2.cvtColor(image_color, cv2.COLOR_BGR2GRAY)  # 灰度图
        image_circle_center, image_dst_mask = self.detect_local_circle(image, mask=True)
        orb = cv2.ORB_create()
        points = []
        for key, value in self.detect_keypoint_images.items():
            image_src_color, image_src_mask = value
            image_src_gray = cv2.cvtColor(image_src_color, cv2.COLOR_BGR2GRAY)  # 灰度图
            keypoint1, desc1 = orb.detectAndCompute(image_src_gray, image_src_mask)
            keypoint2, desc2 = orb.detectAndCompute(image_dst_gray, image_dst_mask)

            desc1 = add_rgb_feat(keypoint1, desc1, value[0])
            desc2 = add_rgb_feat(keypoint2, desc2, image_color)

            bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
            matches = bf.knnMatch(desc1, desc2, k=1)
            for match in matches:
                if len(match) != 0 and match[0].distance < self.keypoint_match_min_dis:
                    point = (int(keypoint2[match[0].trainIdx].pt[0]), int(keypoint2[match[0].trainIdx].pt[1]))
                    cv2.circle(image, point, 1, (255, 0, 0), 1)
                    points.append(point)

        points = np.array(points)
        db = DBSCAN(eps=self.dbscan_eps, min_samples=self.dbscan_min_samples).fit(points)
        core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
        core_samples_mask[db.core_sample_indices_] = True
        labels = db.labels_

        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
        keypoint_list = []
        distance_list = []
        for i in range(n_clusters_):
            center = np.mean(points[np.where(labels == i)], axis=0, dtype=np.int32)
            distance = np.sqrt((center[0] - image_circle_center[0]) ** 2 + (center[1] - image_circle_center[1]) ** 2)
            if self.keypoint2circle_center_dis_bound[0] < distance < self.keypoint2circle_center_dis_bound[1]:
                keypoint_list.append(center)
                distance_list.append(distance)

        keypoint_list = np.array(keypoint_list)

        dis_sort_index = np.argsort(distance_list)
        keypoint_list = keypoint_list[dis_sort_index]

        if len(keypoint_list) < 3:
            raise Exception('Not enough keypoint detected')
        keypoint_list = keypoint_list[:3]

        for i in range(len(keypoint_list)):
            cv2.circle(image, keypoint_list[i], 1, (0, 0, 255), 2)
        # img3 = cv2.drawMatchesKnn(img1, keypoint1, img2, keypoint2, matches, img2, flags=2)
        if self.debug:
            cv2.imshow('image', image)
            cv2.waitKey(0)
            cv2.destroyAllWindows()
        return keypoint_list


if __name__ == '__main__':
    matcher = Matcher()
    root = './data/rui_hong_local_circle'
    path_list = os.listdir(root)
    path_list.sort()
    for p in path_list:
        path = os.path.join(root, p)
        color = cv2.imread(path)
        res = matcher.detect_local_keypoint(color)
