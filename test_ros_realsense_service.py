from __future__ import print_function

import time

import message_filters
import sys
import rospy
import cv2
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image, CameraInfo

import queue
import threading
from match import Matcher


class MatchTest:
    def __init__(self):
        self.interThreadQueue = queue.Queue()
        self.bridge = CvBridge()
        self.rgb_sub = message_filters.Subscriber('/camera/d435_color/image_raw', Image)
        self.info_sub = message_filters.Subscriber('camera_info', CameraInfo)

        self.ts = message_filters.TimeSynchronizer([self.rgb_sub], 100)
        self.ts.registerCallback(self.callback)
        self.display_thread = threading.Thread(target=self.__daemon_display_image)
        self.display_thread.start()

        # Match
        self.compute_poseQueue = queue.LifoQueue()
        self.matcher = Matcher()

        self.match_thread = threading.Thread(target=self.__match)
        self.match_thread.start()

    def __daemon_display_image(self):
        while True:
            while self.interThreadQueue.empty() is False:
                window_name, image = self.interThreadQueue.get()
                cv2.imshow(window_name, cv2.resize(image, None, fx=1.0, fy=1.0))
                cv2.waitKey(10)
            rospy.sleep(.01)

    def display_image_on_window(self, image, window_name, resize_x_axis=1., resize_y_axis=1.):
        self.interThreadQueue.put((window_name, cv2.resize(image, None, fx=resize_x_axis, fy=resize_y_axis)))

    def __match(self):
        pass
        # while True:
        #     color = None
        #     while self.compute_poseQueue.empty() is False:
        #         color = self.compute_poseQueue.get()
        #     if color is not None:
        #         self.matcher.detect_global_circle(color)

    def put_image2queue(self, color):
        name = time.time()
        cv2.imwrite('./data/rui_hong_local_circle/' + str(name) + '.png', color)
        self.compute_poseQueue.put(color)

    def callback(self, image):
        cv_rgb_image = None
        try:
            cv_rgb_image = self.bridge.imgmsg_to_cv2(image, "bgr8")
        except CvBridgeError as e:
            print(e)
        self.display_image_on_window(cv_rgb_image, "current color sync")
        self.put_image2queue(cv_rgb_image)
        rospy.sleep(0.2)


def main(args):
    rospy.init_node('image_converter', anonymous=True)
    ic = MatchTest()
    rospy.sleep(1)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main(sys.argv)
