# Circle Match
***
## Description
>Using Hough circle transform and orb feature matching to implement the matching of circular holes and the location of key points
## Installation
* Create conda environment
```angular2html
conda create -n circle_match python=3.8
conda activate circle_match
```
* Install python dependencies
```angular2html
pip install opencv-python sklearn labelImg rospkg
```
## Usage
* Label keypoints on the image
```angular2html
labelImg # select image to label and save in the templates/xml
cp [image_file] templates/image
python xml2mask.py templates/xml/[xml_file]
```
* Run match function
```angular2html
from match import Matcher
matcher = Matcher()
# detect local circle
result = matcher.detect_local_circle(image)
# detect global circle
result = matcher.detect_global_circle(image)
# detect local keypoint
result = matcher.detect_local_keypoint(image)
```
* Test ros realsense service
```angular2html
python test_ros_realsense_service.py
```
