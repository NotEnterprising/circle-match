import os.path as osp

import cv2
import numpy as np
import xml.etree.ElementTree as ET
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("xml_file", type=str)
args = parser.parse_args()

xml_file = args.xml_file
tree = ET.parse(xml_file)
root = tree.getroot()
file_name = root.find("filename").text
width = int(root.find("size").find("width").text)
height = int(root.find("size").find("height").text)
mask = np.zeros((height, width), dtype=np.uint8)
for _object in root.findall("object"):
    category = _object.find("name").text
    xmin = int(_object.find("bndbox").find("xmin").text)
    ymin = int(_object.find("bndbox").find("ymin").text)
    xmax = int(_object.find("bndbox").find("xmax").text)
    ymax = int(_object.find("bndbox").find("ymax").text)
    w = xmax - xmin
    h = ymax - ymin
    coco_box = [max(xmin, 0), max(ymin, 0), min(w, width), min(h, height)]
    mask[ymin:ymax, xmin:xmax] = 255
mask_save_path = osp.join(osp.split(osp.realpath(__file__))[0], 'templates', 'mask', file_name)
cv2.imwrite(mask_save_path, mask)
